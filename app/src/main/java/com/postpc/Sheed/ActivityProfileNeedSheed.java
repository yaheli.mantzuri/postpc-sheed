//package com.postpc.Sheed;
//
//import android.animation.Animator;
//import android.animation.AnimatorListenerAdapter;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.TextView;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.google.android.material.imageview.ShapeableImageView;
//import com.squareup.picasso.Picasso;
//
//import static com.postpc.Sheed.Utils.USER_INTENT_SERIALIZABLE_KEY;
//
//public class ActivityProfileNeedSheed {
//}

package com.postpc.Sheed;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;

import static com.postpc.Sheed.Utils.USER1_TEST;
import static com.postpc.Sheed.Utils.USER_INTENT_SERIALIZABLE_KEY;


public class ActivityProfileNeedSheed extends AppCompatActivity {

    SheedUser currentUser;
    SheedUsersDB db;

    TextView name;
    EditText bio;
    ImageButton edit_button;
    ShapeableImageView img;


    View swipeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_need_sheed);

        final Intent sheedUserIntent = getIntent();
        if (sheedUserIntent != null)
        {
            currentUser = (SheedUser) sheedUserIntent.getSerializableExtra(USER_INTENT_SERIALIZABLE_KEY);
        }


        db = SheedApp.getDB();
//        db.downloadUserAndDo(USER1_TEST, this::fillRhsUser);

        name = findViewById(R.id.name);
        bio = findViewById(R.id.bio);
        edit_button = findViewById(R.id.edit_button);
        img = findViewById(R.id.img);

        fillRhsUser(currentUser);


    }


    void fillRhsUser(SheedUser sheedUser)
    {
        name.setText(sheedUser.firstName);
        Picasso.with(this).load(sheedUser.imageUrl).into(img);

        img.animate().rotationBy(360f).alpha(1 / 0.3f).setDuration(500L).
                setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        img.setVisibility(View.VISIBLE);
                    }
                }).start();

        img.animate().rotationBy(360f).alpha(1 / 0.3f).setDuration(500L).
                setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        img.setVisibility(View.VISIBLE);
                    }
                }).start();

    }




//    void fillRhsUser()
//    {
//        name.setText(currentUser.firstName);
//        Picasso.with(this).load(currentUser.imageUrl).into(img);
//
//        img.animate().rotationBy(360f).alpha(1 / 0.3f).setDuration(500L).
//                setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        img.setVisibility(View.VISIBLE);
//                    }
//                }).start();
//
//        img.animate().rotationBy(360f).alpha(1 / 0.3f).setDuration(500L).
//                setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        img.setVisibility(View.VISIBLE);
//                    }
//                }).start();
//    }

}
