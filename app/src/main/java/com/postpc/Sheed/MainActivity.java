package com.postpc.Sheed;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import static com.postpc.Sheed.Utils.USER1_TEST;
import static com.postpc.Sheed.Utils.USER_INTENT_SERIALIZABLE_KEY;

public class MainActivity extends AppCompatActivity {

    SheedUsersDB db;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        if (db == null)
        {
            db = SheedApp.getDB();
        }

//        String userId = db.getIdFromSP(); //this is the code should run
        String userId = USER1_TEST;
        if (userId == null)
        {
            // go to registration page

            // but for now im going to MatchActivity just for test

//            Intent matchActivityIntent = new Intent(context, MatchActivity.class);
//            startActivity(matchActivityIntent);
//            return;

            Intent matchActivityIntent = new Intent(context, ProfileActivity.class);
            startActivity(matchActivityIntent);
            return;

        }
        else
        {
            db.downloadUserAndDo(userId, sheedUser -> {

//            Intent matchActivityIntent = new Intent(context, MatchActivity.class);
//            matchActivityIntent.putExtra(USER_INTENT_SERIALIZABLE_KEY, sheedUser);
//            startActivity(matchActivityIntent);

//            Intent matchActivityIntent = new Intent(context, ActivityProfileNeedSheed.class);
//            matchActivityIntent.putExtra(USER_INTENT_SERIALIZABLE_KEY, sheedUser);
//            startActivity(matchActivityIntent);

//                Intent matchActivityIntent = new Intent(context, ProfileActivity.class);
//                matchActivityIntent.putExtra(USER_INTENT_SERIALIZABLE_KEY, sheedUser);
//                startActivity(matchActivityIntent);


            Intent matchActivityIntent = new Intent(context, ActivitySignIn.class);
            matchActivityIntent.putExtra(USER_INTENT_SERIALIZABLE_KEY, sheedUser);
            startActivity(matchActivityIntent);

        });


//
//            Intent matchActivityIntent = new Intent(context, ActivitySignIn.class);
//            startActivity(matchActivityIntent);
//            return;


//            Intent matchActivityIntent = new Intent(context, ActivityProfileNeedSheed.class);
//            startActivity(matchActivityIntent);
//            return;
    }
    }
}